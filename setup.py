#!/usr/bin/env python

from setuptools import setup

setup(
    name="arcade-hoglue",
    version="0.1.0",
    description="hotglue etl utilities for arcade",
    author="hotglue",
    url="https://hotglue.xyz",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    install_requires=[
        "pandas",
    ],
    packages=["arcade_hotglue"],
)
