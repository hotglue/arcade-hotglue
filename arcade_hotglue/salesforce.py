import pandas as pd
from arcade_hotglue import salesforce_metrics


def process_contacts_activites(contacts_df):
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)
    
    acc_df = acc_df.append(salesforce_metrics.process_contacts(contacts_df.copy()))
    

    return acc_df


def process_opportunities_activites(opp_df):
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)
    
    acc_df = acc_df.append(salesforce_metrics.process_opportunities(opp_df))
    acc_df = acc_df.append(salesforce_metrics.process_lost_opp(opp_df))
    acc_df = acc_df.append(salesforce_metrics.process_closed_won_opp(opp_df))
    acc_df = acc_df.append(salesforce_metrics.process_amount_closed_won_opp(opp_df))

    return acc_df

# ALL THIS BLOCK IS CUSTOM METRICS VALID ONLY FOR ARCADE
def process_account_opportunities_activites(accounts_df, opp_df):
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)
    accounts = salesforce_metrics.filter_accounts(accounts_df)

    #if (accounts is not None) and ("AccountId" in opp_df.columns):
        # Filter the opportunities to only get matching accounts
        # wireless_sko = opp_df.loc[opp_df['AccountId'].isin(accounts["Id"])]
        # Filter the opportunities
        # acc_df = acc_df.append(salesforce_metrics.wireless_sko(wireless_sko))

    #if "AccountId" in opp_df.columns:
    #    # Merge account and Opportunities dfs
    #    opp_acc_df = pd.merge(opp_df, accounts_df, left_on="AccountId", right_on="Id", suffixes=(None, "_acc"))
               
    #    # Closed Won iQmetrix Opportunity
    #    acc_df = acc_df.append(salesforce_metrics.process_iqmetrix_won(opp_acc_df))
    #    # Closed Won iQmetrix Aktivate Opportunity Opportunity
    #    acc_df = acc_df.append(salesforce_metrics.process_iqmetrix_a_won(opp_acc_df))
    #    # Closed Won RTPOS Opportunity
    #    acc_df = acc_df.append(salesforce_metrics.process_rtpos_won(opp_acc_df))
    #    # Closed Won Sheet Integration Opportunity
    #    acc_df = acc_df.append(salesforce_metrics.process_sheet_won(opp_acc_df))
    #    # Closed Won Salesforce Opportunity
    #    acc_df = acc_df.append(salesforce_metrics.process_salesforce_won(opp_acc_df))
    #    # Closed Won Boost Mobile Opportunity
    #    acc_df = acc_df.append(salesforce_metrics.process_boost_won(opp_acc_df))
    return acc_df


def process_lead_activites(lead_df):
    # Activites df
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)
    acc_df = acc_df.append(salesforce_metrics.process_created_leads(lead_df))
    acc_df = acc_df.append(salesforce_metrics.process_converted_leads(lead_df))
    return acc_df


def process_events_activites(events_df, accounts_df):
    # Activites df
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)
    events_df = events_df[~events_df["AccountId"].isna()]

    # Merge events with opportunity to get the meetings
    acc_events = pd.merge(events_df, accounts_df, left_on="AccountId", right_on="Id", suffixes=(None, "_acc"))
    
    # CUSTOM METRICS, VALID ONLY FOR ARCADE
    # Filter the booked demos
    # acc_df = acc_df.append(salesforce_metrics.process_booked_demo(acc_events))
    # Filter the completed demos
    #acc_df = acc_df.append(salesforce_metrics.process_sdr_booked_demo(acc_events))
    # Filter Discovery Calls
    #acc_df = acc_df.append(salesforce_metrics.process_discovery_calls_booked(acc_events))
    # Filter the completed demos
    #acc_df = acc_df.append(salesforce_metrics.process_completed_demo(acc_events))
    # Filer the events
    #acc_df = acc_df.append(salesforce_metrics.process_outbound_booked_deep_dive_discoveries(acc_events))
    # Filter the completed demos
    #acc_df = acc_df.append(salesforce_metrics.process_mvq(acc_events))
    
    return acc_df


def process_task_activites(task_df):
    # Activites df
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)

    acc_df = acc_df.append(salesforce_metrics.process_sent_emails(task_df))
    acc_df = acc_df.append(salesforce_metrics.process_made_calls(task_df))
    acc_df = acc_df.append(salesforce_metrics.process_tasks_completed(task_df))
    acc_df = acc_df.append(salesforce_metrics.process_meetings_booked(task_df))

    return acc_df


def process_case_activites(case_df):
    # Activites df
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)
    acc_df = acc_df.append(salesforce_metrics.process_closed_cases(case_df))

    return acc_df


def process_case_history_activites(case_history, case_df):
    # Activites df
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)
    
    # CUSTOM METRIC ONLY VALID FOR ARCADE
    #if case_history is not None:
    #    acc_df = acc_df.append(salesforce_metrics.process_support_NoDev_cases(case_df, case_history))
    
    return acc_df


def process_project_activites(project_owner_df, project_engineer_df):
    # Activites df
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)
    
    # CUSTOM METRICS ONLY VALID FOR ARCADE
    # by OwnerID
    #acc_df = acc_df.append(salesforce_metrics.process_closed_projects(project_owner_df))
    # by Engineer
    #acc_df = acc_df.append(salesforce_metrics.process_closed_projects(project_engineer_df))
    # by OwnerID
    #acc_df = acc_df.append(salesforce_metrics.process_completed_upgrades_projects(project_owner_df))
    # by Engineer
    #acc_df = acc_df.append(salesforce_metrics.process_completed_upgrades_projects(project_engineer_df))

    return acc_df


def process_milestones_activites(milestones_df):
    # Activites df
    acc_df = pd.DataFrame(columns=salesforce_metrics.acc_cols)

    # CUSTOM METRICS ONLY VALID FOR ARCADE
    # acc_df = acc_df.append(salesforce_metrics.process_completed_milestones(milestones_df))
    
    return acc_df
