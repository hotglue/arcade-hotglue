import pandas as pd
import functools
from datetime import datetime, timedelta

acc_cols = [
    "UserId",
    "ActivityName",
    "ActivityVerb",
    "ActivityValue",
    "SourceTransactionId",
    "SourceTransactionCreateDate",
    "SourceTransactionLastModDate"
]

def ignore_failures(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except KeyError:
            print("Missing custom field")
        except TypeError:
            print("Missing data")
    return wrapper

# ok default
@ignore_failures
def process_contacts(contacts_df):
    # Add Activity Name to output
    contacts_df["ActivityName"] = "Contacts"
    contacts_df["ActivityVerb"] = "Created"
    contacts_df["ActivityValue"] = (~contacts_df["IsDeleted"]).astype(int)
    # Rename columns
    contacts_df = contacts_df.rename(columns={
        'Id': 'SourceTransactionId',
        'CreatedById': 'UserId',
        'LastModifiedDate': 'SourceTransactionLastModDate',
        'CreatedDate': 'SourceTransactionCreateDate'
    })
    # Only select relevant cols
    contacts_df = contacts_df[acc_cols]

    return contacts_df.drop_duplicates()

# ok default
@ignore_failures
def process_opportunities(opp_df):
    created_opps = opp_df.copy()

    # Add Activity Name to output
    created_opps["ActivityName"] = "Opportunities"
    created_opps["ActivityVerb"] = "Created"
    created_opps["ActivityValue"] = (~created_opps["IsDeleted"]).astype(int)

    # Only select relevant cols
    return created_opps[acc_cols]

# ok default
@ignore_failures
def process_lost_opp(opp_df):
    lost_df = opp_df[(opp_df['IsWon'] == False) & (opp_df['IsClosed'] == True)]
    # Add Activity Name to output
    lost_df["ActivityName"] = "Opportunities"
    lost_df["ActivityVerb"] = "Closed Lost"
    lost_df["ActivityValue"] = (~lost_df["IsDeleted"]).astype(int)

    # Only select relevant cols
    return lost_df[acc_cols]

# ok default
@ignore_failures
def process_closed_won_opp(opp_df):
    won_df = opp_df[(opp_df['IsWon'] == True) & 
        (opp_df['IsClosed'] == True)]

    # Need a different Create Date.
    # Change back to original CreateDate
    won_df.rename(columns={
        'SourceTransactionCreateDate': 'CreatedDate',
        'UserId': 'CreatedById'
    }, inplace=True)

    # Rename 
    won_df.rename(columns={
        'CloseDate': 'SourceTransactionCreateDate',
        'OwnerId': 'UserId'
    }, inplace=True)

    # Add hours to the date to avoid UTC problem in Arcade
    won_df['SourceTransactionCreateDate'] = won_df['SourceTransactionCreateDate'].replace('00:00:00', '06:00:00', regex=True)

    # Add Activity Name to output
    won_df["ActivityName"] = "Opportunities"
    won_df["ActivityVerb"] = "Closed Won"
    won_df["ActivityValue"] = (~won_df["IsDeleted"]).astype(int)
    # Only select relevant cols
    return won_df[acc_cols]

# ok default
@ignore_failures
def process_amount_closed_won_opp(opp_df):
    amount_won = opp_df[(opp_df['IsWon'] == True) & 
        (opp_df['IsClosed'] == True)]

        # Need a different Create Date.
    # Change back to original CreateDate
    amount_won.rename(columns={
        'SourceTransactionCreateDate': 'CreatedDate',
        'UserId': 'CreatedById',
    }, inplace=True)

    # Rename 
    amount_won.rename(columns={
        'CloseDate': 'SourceTransactionCreateDate',
        'OwnerId': 'UserId',
    }, inplace=True)

    # Add hours to the date to avoid UTC problem in Arcade
    amount_won['SourceTransactionCreateDate'] = amount_won['SourceTransactionCreateDate'].replace('00:00:00', '06:00:00', regex=True)

    # Add Activity Name to output
    amount_won["ActivityName"] = "Opportunities"
    amount_won["ActivityVerb"] = "Closed Won Value"
    amount_won.rename(columns={'Amount': 'ActivityValue'}, inplace=True)
    # Only select relevant cols
    return amount_won[acc_cols]

# ok defualt
@ignore_failures
def process_created_leads(lead_df):
    created_leads = lead_df.copy()
    # Add Activity Name to output
    created_leads["ActivityName"] = "Leads"
    created_leads["ActivityVerb"] = "Created"
    created_leads["ActivityValue"] = (~created_leads["IsDeleted"]).astype(int)
    # Only select relevant cols
    return created_leads[acc_cols]

# ok default
@ignore_failures
def process_converted_leads(lead_df):
    converted_lead = lead_df[lead_df['IsConverted'] == True]
    
    # Need a different Create Date.
    # Change back to original CreateDate
    converted_lead.rename(columns={
        'SourceTransactionCreateDate': 'CreatedDate'
    }, inplace=True)

    # Rename 
    converted_lead.rename(columns={
        'ConvertedDate': 'SourceTransactionCreateDate'
    }, inplace=True)

    # Add Activity Name to output
    converted_lead["ActivityName"] = "Leads"
    converted_lead["ActivityVerb"] = "Converted"
    converted_lead["ActivityValue"] = (~converted_lead["IsDeleted"]).astype(int)
    # Only select relevant cols
    return converted_lead[acc_cols]

# ok default
@ignore_failures
def process_sent_emails(task_df):
    sent_emails = task_df[task_df['Type'] == "Email"]
    # Add Activity Name to output
    sent_emails["ActivityName"] = "Emails"
    sent_emails["ActivityVerb"] = "Sent"
    sent_emails["ActivityValue"] = 1
    # Only select relevant cols
    return sent_emails[acc_cols]

# ok default
@ignore_failures
def process_made_calls(task_df):
    made_calls = task_df[task_df['Type'] == "Call"]
    # Add Activity Name to output
    made_calls["ActivityName"] = "Calls"
    made_calls["ActivityVerb"] = "Made"
    made_calls["ActivityValue"] = 1
    # Only select relevant cols
    return made_calls[acc_cols]

# ok default
@ignore_failures
def process_tasks_completed(task_df):
    tasks_completed = task_df[((task_df['Type'] == "Task") & (task_df['IsClosed']))]
    # Add Activity Name to output

        # Need a different Create Date.
    # Change back to original CreateDate
    tasks_completed.rename(columns={
        'SourceTransactionCreateDate': 'CreatedDate'
    }, inplace=True)

    # Rename 
    tasks_completed.rename(columns={
        'LastModifiedDate': 'SourceTransactionCreateDate'
    }, inplace=True)

    tasks_completed["ActivityName"] = "Tasks"
    tasks_completed["ActivityVerb"] = "Completed"
    tasks_completed["ActivityValue"] = 1
    # Only select relevant cols
    return tasks_completed[acc_cols]

# ok default
@ignore_failures
def process_meetings_booked(task_df):
    meetings_booked = task_df[task_df['Type'] == "Meeting"]
    # Add Activity Name to output
    meetings_booked["ActivityName"] = "Meetings"
    meetings_booked["ActivityVerb"] = "Booked"
    meetings_booked["ActivityValue"] = 1
    # Only select relevant cols
    return meetings_booked[acc_cols]

# ok default
@ignore_failures
def process_closed_cases(case_df):
    closed_cases = case_df[case_df['IsClosed'] == True]
    # Add Activity Name to output
    closed_cases["ActivityName"] = "Cases"
    closed_cases["ActivityVerb"] = "Closed"
    closed_cases["ActivityValue"] = (~closed_cases["IsDeleted"]).astype(int)
    # Only select relevant cols
    return closed_cases[acc_cols]
