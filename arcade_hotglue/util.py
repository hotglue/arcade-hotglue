import os
import pandas as pd
from datetime import date

def get_snapshot(snapshot_dir, stream):
    snap_path = f"{snapshot_dir}/{stream}.snapshot.csv"

    # Ensure file exists
    if os.path.isfile(snap_path) is False:
        return None

    # Read the old snapshot, if present
    snap_df = pd.read_csv(snap_path)

    return snap_df


def update_snapshot(snapshot_dir, stream, key, data_df, persist=True, override=False,
                    drop_column=None, sort_columns=None, drop_duplicates=True):
    snap_df = data_df

    if drop_duplicates:
        snap_df = snap_df.drop_duplicates(key, keep="last")

    if not override:
        # Get the old snapshot dataframe
        psnap_df = get_snapshot(snapshot_dir, stream)

        if psnap_df is not None:
            # Combine with prior snapshot
            snap_df = snap_df.set_index(
                key).combine_first(psnap_df.set_index(key))
            snap_df = snap_df.reset_index()
    if drop_column is not None:
        snap_df = snap_df.drop(columns=drop_column, errors='ignore')
    if persist:
        # Save this snapshot in correct spot
        snap_path = f"{snapshot_dir}/{stream}.snapshot.csv"
        snap_df.to_csv(snap_path, index=False)

    if sort_columns is not None:
        snap_df.sort_values(by=sort_columns)

    return snap_df


def week_from_date(date_object):
    date_ordinal = date_object.toordinal()
    year = date_object.year
    week = ((date_ordinal - _week1_start_ordinal(year)) // 7) + 1
    if week >= 52:
        if date_ordinal >= _week1_start_ordinal(year + 1):
            year += 1
            week = 1
    return year, week


def _week1_start_ordinal(year):
    jan1 = date(year, 1, 1)
    jan1_ordinal = jan1.toordinal()
    jan1_weekday = jan1.weekday()
    week1_start_ordinal = jan1_ordinal - ((jan1_weekday + 1) % 7)
    return week1_start_ordinal